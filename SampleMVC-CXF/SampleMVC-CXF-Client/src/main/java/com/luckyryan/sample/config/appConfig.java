package com.luckyryan.sample.config;

import com.luckyryan.sample.exception.ExceptionHandler;
import com.luckyryan.sample.ws.SampleServiceREST;
import com.luckyryan.sample.ws.SampleServiceSOAP;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxws.JaxWsClientFactoryBean;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.hdiv.web.validator.EditableParameterValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Validator;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.ws.rs.client.Client;
import javax.ws.rs.ext.RuntimeDelegate;
import java.util.ArrayList;
import java.util.List;

/**
 * User: ryan
 * Date: 2/7/13
 */
@EnableWebMvc
@ComponentScan(basePackages = {"com.luckyryan.sample.model", "com.luckyryan.sample.webapp", "com.luckyryan.sample.service"})
@Configuration
public class appConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/assets/**").addResourceLocations("classpath:/META-INF/resources/webjars/").setCachePeriod(31556926);
        registry.addResourceHandler("/css/**").addResourceLocations("/css/").setCachePeriod(31556926);
        registry.addResourceHandler("/img/**").addResourceLocations("/img/").setCachePeriod(31556926);
        registry.addResourceHandler("/js/**").addResourceLocations("/js/").setCachePeriod(31556926);
    }

    // This overrides the javax field validator with HDIV
    // Remove this block to disable HDIV field validation
    @Override
    public Validator getValidator() {
        return getParameterValidator();
    }

    @Bean
    public EditableParameterValidator getParameterValidator() {
        return new EditableParameterValidator();
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Bean
    public InternalResourceViewResolver getInternalResourceViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/pages/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    public JacksonJsonProvider getJacksonJsonProvider() {
        return new JacksonJsonProvider();
    }

    @Bean
    public ExceptionHandler getExceptionHandler() {
        return new ExceptionHandler();
    }

    @Bean
    public SampleServiceREST getSampleServiceRestClient() {
        List providers = new ArrayList();
        providers.add(getJacksonJsonProvider());
        providers.add(getExceptionHandler());
        return JAXRSClientFactory.create("http://localhost:8090/services/rest",SampleServiceREST.class,providers);
    }

    @Bean
    public SampleServiceSOAP getSampleServiceSoapClient() {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(SampleServiceSOAP.class);
        factory.setAddress("http://localhost:8090/services/soap");
        return (SampleServiceSOAP) factory.create();
    }

}



