package com.luckyryan.sample.exception;

import org.apache.cxf.jaxrs.client.ResponseExceptionMapper;

import javax.ws.rs.core.Response;


public class ExceptionHandler implements ResponseExceptionMapper {

    @Override
    public Throwable fromResponse(Response response) {
        throw new InvalidUserException(response.getHeaderString("exception"));
    }
}